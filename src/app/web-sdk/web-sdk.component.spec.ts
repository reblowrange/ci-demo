import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebSdkComponent } from './web-sdk.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
declare const WebVideoCtrl: any; // Import the WebVideoCtrl object from the SDK

describe('WebSdkComponent', () => {
  let component: WebSdkComponent;
  let fixture: ComponentFixture<WebSdkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ WebSdkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WebSdkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
