import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { WebSdkComponent } from './web-sdk/web-sdk.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
declare const WebVideoCtrl: any; // Import the WebVideoCtrl object from the SDK

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [
        AppComponent,
        WebSdkComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

});
